<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;


require_once(__DIR__.'/../app/ext/vendor/autoload.php');
require_once(__DIR__.'/../app/controller/WordsController.php');

$app = AppFactory::create();

$app->addRoutingMiddleware();
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

$app->get('/words', function (Request $request, Response $response, $args) {
  $cnt = new WordsController();
    $words = $cnt->getWordList();

    $arrayW = array();
    foreach ($words as $w) {
      $arrayW[] = $w->toArray();
    }

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($arrayW));
    return $newR;
});


/*
$app->get('/weapons/{wid}',function(Request $request, Response $response, array $args) {
  $wid = $args['wid'];
  $cnt = new WeaponController();

  $wobj = $cnt->detailsAction($wid);

  $newR = $response->withHeader('Content-type', 'application/json');
  $newR->getBody()->write(json_encode($wobj->toArray()));
  return $newR;
});

$app->post('/weapons',function(
        Request $request, Response $response, array $args) {
    $wjson = $request->getParsedBody();
    $cnt = new WeaponController();

    $newwp = $cnt->createAction($wjson['tipo'], $wjson['nombre'],
        $wjson['material'], $wjson['origen'], $wjson['peso'],
        $wjson['filo'], $wjson['media']);

    $newR = $response->withHeader('Content-type', 'application/json');
    $newR->getBody()->write(json_encode($newwp->toArray()));
    return $newR;
});

$app->delete('/weapons/{wid}',function(Request $request, Response $response, array $args) {
  $wid = $args['wid'];
  $cnt = new WeaponController();

  $wobj = $cnt->deleteAction($wid);

  $newR = $response->withHeader('Content-type', 'application/json');
  $newR->getBody()->write(json_encode($wobj->toArray()));
  return $newR;
});

$app->put('/weapons/{wid}',function(Request $request, Response $response, array $args) {
  $wid = $args['wid'];
  $wjson = $request->getParsedBody();
  $cnt = new WeaponController();

  $newwp = $cnt->createAction($wjson['tipo'], $wjson['nombre'],
      $wjson['material'], $wjson['origen'], $wjson['peso'],
      $wjson['filo'], $wjson['media']);

  $wobj = $cnt->deleteAction($wid);

  $newR = $response->withHeader('Content-type', 'application/json');
  $newR->getBody()->write(json_encode($wobj->toArray()));
  return $newR;
});
*/

$app->run();
