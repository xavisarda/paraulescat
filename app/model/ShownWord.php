<?php

  /**
   * Class for the shown words in the app
   */
  class ShownWord{

    private $_word;
    private $_wordLgth;

    public function __construct($wrd, $wrdlgth = null){
      $this->setWord($wrd);
      if($wrdlgth == null){
        $this->setWordLgth(strlen($wrd));
      }else{
        $this->setWordLgth($wrdlgth);
      }
    }

    /**
     * Get word text value
     * @return String word text
     */
    public function getWord(){
      return $this->_word;
    }

    /**
     * Set word text value
     * @param String word value
     */
    public function setWord($value){
      $this->_word = $value;
    }

    /**
     * Get word lengt as integer
     * @return int word lenght
     */
    public function getWordLgth(){
      return $this->_wordLgth;
    }

    /**
     * Set word length
     * @param int word length
     */
    public function setWordLgth($value){
      $this->_wordLgth = $value;
    }

    public function toArray(){
      $w = array();
      $w['word'] = $this->getWord();
      $w['length'] = $this->getWordLgth();

      return $w;
    }
  }
