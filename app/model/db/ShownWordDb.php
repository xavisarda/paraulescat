<?php

require_once(__DIR__.'/../ShownWord.php');

  class ShownWordDb extends SQLite3{

    public function __construct(){
      $this->open(__DIR__.'/../../data/paraulescat');
    }

    public function getWordList(){
      $sql = "SELECT * FROM words";
      $ret = $this->query($sql);

      $words = array();
      while( $worow = $ret->fetchArray(SQLITE3_ASSOC) ){
        $words[] = new ShownWord($worow['wrd'],$worow['wrd_lgth']);
      }

      return $words;
    }
  }
  